from django.test import TestCase, Client, RequestFactory
from django.urls import resolve
from django.http import HttpRequest

from .views import index,data

class Testing(TestCase):
    
    def test_url_(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_using_base_template(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'book.html')

    def test_using_index_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, index)

    def test_url_data(self):
        response = Client().get('/book/data?q=')
        self.assertEqual(response.status_code, 200)

    def test_using_data_func(self):
        response = resolve('/book/data')
        self.assertEqual(response.func, data)
        