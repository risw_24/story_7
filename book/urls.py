from django.contrib import admin
from django.urls import path
from .views import index,data

app_name = 'story8'

urlpatterns = [
    path('',index,name="book"),
    path('data',data),
]