from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request,"book.html")

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    hasil = requests.get(url).content
    data= json.loads(hasil)
    return JsonResponse(data,safe=False)
    