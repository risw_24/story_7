from django.test import TestCase, Client, RequestFactory
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


from .views import index,register,login1,logout1

class Testing(TestCase):
    
    def test_url_(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_base_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_landing_page(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        isi = "Mohammad Riswanda Alifarahman"
        self.assertIn(isi,html_response)
    
    def test_using_register_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)
    
    def test_using_login_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login1)
    
    def test_using_logout_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout1)

    def test(self):
        response = Client().post('/login/', {'username': 'john', 'password': 'smith'})
        self.assertEqual(response.status_code, 200)

    def test_registrasi(self):
        response = Client().post('/register/', {'username': 'john', 'password1': 'smith', 'password2' : 'smith'})
        self.assertEqual(response.status_code, 200)