from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from .forms import CreateUserForm


def index(request):
    return render(request,"base.html")

def register(request):
    if request.user.is_authenticated:
        return redirect("home")

    else:
        form = UserCreationForm()
        if(request.method=='POST'):
            form = UserCreationForm(request.POST)
            if form.is_valid():
                form.save() 
                return redirect("login")
        return render(request,"register.html", {"form":form})

def login1(request):
    if request.user.is_authenticated:
        return redirect("home")
    else:
        if request.method=="POST":
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request,username=username, password=password)
            if user is not None:
                login(request,user)
                return redirect('home')
        
        return render(request,"login.html")

def logout1(request):
    logout(request)
    return redirect('home')

