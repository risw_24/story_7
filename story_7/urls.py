"""story_7 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import index,register,login1,logout1
from django.conf.urls import include
from django.urls import re_path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/',register, name="register"),
    path('login/',login1, name='login'),
    path('logout/',logout1, name='logout'),
    path('',index, name="home"),
    re_path(r'book/', include('book.urls')),
]

